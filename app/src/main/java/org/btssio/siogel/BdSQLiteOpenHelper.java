package org.btssio.siogel;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BdSQLiteOpenHelper extends SQLiteOpenHelper {
    private String requete="create table client ("
            +"id INTEGER,"
            +"nom TEXT NOT NULL,"
            +"prenom TEXT NOT NULL,"
            +"adresse TEXT NOT NULL,"
            +"codepostal TEXT NOT NULL,"
            +"ville TEXT NOT NULL,"
            +"telephone TEXT NOT NULL,"
            +"datecommande TEXT NOT NULL,"
            +"heuresouhaitliv TEXT,"
            +"heurereellelivraison TEXT,"
            +"livraison TEXT,"
            +"signature64 TEXT,"
            +"etat INTEGER);";

    private String requete2="create table livreur ("
            +"id INTEGER,"
            +"nom TEXT NOT NULL,"
            +"prenom TEXT NOT NULL,"
            +"mdp TEXT NOT NULL);";

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(requete);
        db.execSQL(requete2);

        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('1','Richard','Camembert','30 rue Charles Gounod','72700','Allonnes','0243834201','dans 3 jours','15heures','?','livré','MR Camembert','1');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('2','Paul','Frometon','31 place de la Republique','72000','Le Mans','0243876060','dans 5 jours','1heures','?','pas livré','MR Frometon','0');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
               "values('3','Socrate','Chèvre','47 rue du Ribay','72700','Le Mans','0243203388','dans 1 jours','16heures','?','livré','MR Chèvre','1');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                        "values('4','Lise','Roquefort','rue du Mail','72700','Allonnes','0243278673','dans 0 jours','18heures','?','livré','Mme Roquefort','9');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                        "values('5','Marion','Brie','9 avenue du Dr Jean Mac','72100','Le Mans','0252920050','dans 13 jours','15heures','?','livré','Mme Brie','1');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('6','Manon','Caramel','9 rue Ernest Sylvain Bollée','72230','Arnage','0892788052','dans 4 jours','9heures','?','livré','Mme Caramel','3');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('7','Chris','Opticien','Z.I Sud, rue Antoine Becquerel','72000','Le Mans','0892052828','dans 3 jours','19heures','?','livré','Mr Opticien','2');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('8','Tom','Frumou','88 avenue Pierre Piffault','72100','Le Mans','0243857515','dans 7 jours','8heures','?','livré','Mr Frumou','1');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('9','Bruno','Brun','9 rue Joseph Marie Jacquart','72100','Le Mans','0243407020','dans 4 jours','15heures','?','livré','Mr Brun','2');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('10','Lucie','Luc','3 Boulevard Pierre Lefaucheux','72100','Le Mans','0243727233','dans 10 jours','18heures','?','livré','Mme Luc','3');");
        db.execSQL("insert into livreur (id,nom,prenom,mdp )"
        +"values('1','Akas','Paul','mdp1');");
        db.execSQL("insert into livreur (id,nom,prenom,mdp )"
                +"values('2','Bruil','Pierre','mdp2');");
        db.execSQL("insert into livreur (id,nom,prenom,mdp )"
                +"values('3','Merica','Jack','mdp3');");








        //ContentValues value = new ContentValues();
        //value.put("nomV", "Badoz");
        //value.put("niveauV",50);
        //db.insert("viticulteur", null, value);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }
    public BdSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        // TODO Auto-generated constructor stub
    }
}

