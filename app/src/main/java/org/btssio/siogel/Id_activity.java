package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Id_activity  extends Activity implements  android.view.View.OnClickListener{

    TextView nom, prenom;
    LivreurDAO livreurDAO;
    int id;
    String b,c2,n;
    EditText mdp;
    Livreur l;
    private Button connection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_id_activity);
        livreurDAO = new LivreurDAO(this);
        id = this.getIntent().getExtras().getInt("id");
        l = livreurDAO.getLivreur(id);
        b = l.getNom();
        c2 = l.getPrenom();
        n= l.getMDP();
        Log.i("prenom",c2);
        nom = (TextView) this.findViewById(R.id.nom);
        nom.setText(" " + b + " ");
        prenom = (TextView) this.findViewById(R.id.prenomL);
        prenom.setText(" " + c2 + " ");
        connection= (Button) findViewById(R.id.buttonC);
        connection.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        mdp = (EditText) this.findViewById(R.id.mdpA);
        if (n.equals(mdp.getText().toString())  ){
            Log.i("connecté","connecté");
        }

    }
}