package org.btssio.siogel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class LivreurAdapter extends BaseAdapter {
    List<Livreur> listeLivreur;
    LayoutInflater layoutInflater;
    @Override
    public int getCount() {

        return listeLivreur.size();
    }

    @Override
    public Object getItem(int i) {
        return listeLivreur.get(i);
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Conteneur contenu;
        if (convertView == null){
            contenu = new Conteneur();
            convertView = layoutInflater.inflate(R.layout.vue_livreur,null);
            contenu.Id=(TextView) convertView.findViewById(R.id.id);
            contenu.Nom=(TextView) convertView.findViewById(R.id.nom);
            contenu.Prenom=(TextView) convertView.findViewById(R.id.prenom);



            convertView.setTag(contenu);

        }
        else {
            contenu = (Conteneur) convertView.getTag();
        }
        contenu.Id.setText(""+listeLivreur.get(position).getId());
        contenu.Nom.setText(""+listeLivreur.get(position).getNom());
        contenu.Prenom.setText((""+listeLivreur.get(position).getPrenom()));

        return convertView;
    }
    public LivreurAdapter(Context context, List<Livreur> listeLivreur)
    {
        this.layoutInflater = LayoutInflater.from(context);
        this.listeLivreur = listeLivreur;
    }
    public class Conteneur {
        TextView Id;
        TextView Nom;
        TextView Prenom;

    }

}
