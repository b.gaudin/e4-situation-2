package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.List;

public class AfficheListeLivreurs extends Activity  implements AdapterView.OnItemClickListener {
    ListView listView;
    List<Livreur> listeLivreur;
    LivreurDAO unlivreur;
    Intent in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_liste_clients);

        unlivreur = new LivreurDAO(this);

        listeLivreur = unlivreur.getLivreurs();

        listView=(ListView)findViewById(R.id.lvliste);

        LivreurAdapter livreurAdapter = new LivreurAdapter(this,listeLivreur);


        listView.setAdapter(livreurAdapter);

        listView.setOnItemClickListener(this);

    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        in = new Intent(this,Id_activity.class);
        in.putExtra("id",listeLivreur.get(position).getId());
        this.startActivity(in);


    }
}