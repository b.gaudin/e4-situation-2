package org.btssio.siogel;

public class Livreur {
    private String nom,prenom,mdp;
    private int id;


    public Livreur(){
        id =0;
        nom="Marcel";
        prenom="Proust";
        mdp="mdp";
    }
    public Livreur(int id, String nom,String prenom,String mdp){
        this.id=id;
        this.nom=nom;
        this.prenom=prenom;
        this.mdp=mdp;
    }
    public int getId() {
        return id;
    }



    public String getNom() {
        return nom;
    }



    public String getPrenom() {
        return prenom;
    }

    public String getMDP() { return mdp;}
    @Override
    public String toString() {
        return "Livreur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", mdp='" + mdp + '\'' +

                '}';
    }
}
