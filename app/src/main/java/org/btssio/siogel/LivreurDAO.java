package org.btssio.siogel;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

public class LivreurDAO {

    private static String base = "BDClient";
    private static int version = 2;
    private BdSQLiteOpenHelper accesBD;

    public LivreurDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }
    public  Livreur getLivreur(int idC){
        Livreur leLivreur = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from livreur where id="+idC+";",null);
        //Log.i("test","tes1");
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            leLivreur = new Livreur(idC,curseur.getString(1), curseur.getString(2),curseur.getString(3)
                    );
        }

        return leLivreur;
    }



    public ArrayList<Livreur> getLivreurs(){
        Cursor curseur;
        String req = "select * from livreur ";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToLivreurArrayList(curseur);
    }





    private ArrayList<Livreur> cursorToLivreurArrayList(Cursor curseur){
        ArrayList<Livreur> listeLivreur = new ArrayList<Livreur>();
        int id;
        String nom;
        String prenom;
        String mdp;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            id = curseur.getInt(0);
            nom = curseur.getString(1);
            prenom = curseur.getString(2);
            mdp = curseur.getString(3);

            listeLivreur.add(new Livreur(id,nom,prenom,mdp));
            curseur.moveToNext();
        }

        return listeLivreur;
    }
}
