package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.List;

public class AfficheListeClients extends Activity  implements AdapterView.OnItemClickListener {
ListView listView;
List<Client> listeClients;
ClientDAO unclient;
Intent in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_liste_clients);

        unclient = new ClientDAO(this);

        listeClients = unclient.getClients();

        listView=(ListView)findViewById(R.id.lvliste);

        ClientAdapter clientAdapter = new ClientAdapter(this,listeClients);


        listView.setAdapter(clientAdapter);

        listView.setOnItemClickListener(this);

    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        in = new Intent(this,ModificationClient.class);
        in.putExtra("id",listeClients.get(position).getIdentifiant());
        this.startActivity(in);
        //Toast.makeText(getApplicationContext(),"Choix : "+listeClients.get(position).getIdentifiant(), Toast.LENGTH_LONG).show();

    }
}